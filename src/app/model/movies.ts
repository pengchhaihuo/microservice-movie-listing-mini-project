export interface Movies{
  id:Number;
  title:String
  description: String;
  playTime:Number;
  rate:Number;
  movieUrl:String;
  releaseYear: String;
  coverImgUrl:String;
}
