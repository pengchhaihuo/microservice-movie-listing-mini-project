import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminsComponent } from './admins/admins.component';
import { AdminsModule } from './admins/admins.module';
import { AppRoutingModule } from './app-routing.module';
import {UserModule} from "./user/user.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthorizationComponent } from './authorization/authorization.component';
import { AuthorizeModule } from './authorization/authorize.module';
import { AuthGuard } from './authorization/guard/auth.guard';


@NgModule({
  declarations: [
    AppComponent,
    AdminsComponent,
    AuthorizationComponent,
  ],
  imports: [
    BrowserModule,
    AdminsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    UserModule,
    NgbModule,
    AuthorizeModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})


export class AppModule {
}
