import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  signUpForm!:FormGroup;

  constructor(
    private route: Router,
    private routes: ActivatedRoute,
    private fb : FormBuilder
  ) { }

  ngOnInit(): void {
      this.signUpForm = this.fb.group({
          username: new FormControl('',[Validators.required]),
          email: new FormControl('',[Validators.required,Validators.email]),
          password: new FormControl('',Validators.required)
      })
  }

  get getSignupForm(){
    return this.signUpForm as FormGroup
  }

  get getUsername(){
    return this.signUpForm.get('username')
  }

  get getEmail(){
    return this.signUpForm.get('email')
  }

  get getPassword(){
    return this.signUpForm.get('password')
  }

  goToLogin() {
    this.route.navigate(['/authorize/login'])
  }

  onSubmit(){
    console.log("user: ",this.getSignupForm.value);
  }


}
