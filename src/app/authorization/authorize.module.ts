import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { MatDividerModule } from '@angular/material/divider'
import { MatInputModule } from '@angular/material/input'
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../../app/app-routing.module';
import {MatTabsModule} from '@angular/material/tabs';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component'
import { FlexLayoutModule} from '@angular/flex-layout'
import { MatCardModule } from '@angular/material/card'
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './guard/auth.guard';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent
  ],
  imports: [
  HttpClientModule,

    MatDividerModule,
    MatCardModule,
    FlexLayoutModule,
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTabsModule,
    
    ReactiveFormsModule,
    AppRoutingModule
  ],
  exports:[
    HttpClientModule,

    MatDividerModule,
    MatCardModule,
    FlexLayoutModule,
    LoginComponent,
    RegisterComponent,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatTabsModule
  ],
  providers:[AuthGuard]
})
export class AuthorizeModule { }
