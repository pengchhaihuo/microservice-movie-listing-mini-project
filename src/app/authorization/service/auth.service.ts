import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  private _registerUrl = "http://localhost:4200/authorize/register";
  private _loginUrl = "http://localhost:4200/authorize/login"

  registerUser(user: any) {
    return this.http.post<any>(this._registerUrl, user);
  }

  loginUser(user: any) {
    return this.http.post<any>(this._loginUrl, user);
  }

  // get token from local storage
  loggedIn() {
    return localStorage.getItem("token");
  }

}
