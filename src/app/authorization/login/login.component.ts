import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm!:FormGroup;

  constructor(
    private route: Router,
    private routes: ActivatedRoute,
    private fb : FormBuilder
  ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
        username: new FormControl('',[Validators.required]),
        password: new FormControl('',[Validators.required])
    })
  }

  get getLoginForm(){
    return this.loginForm as FormGroup
  }

  get getUsername(){
    return this.getLoginForm.get('username')
  }

  get getPassword(){
    return this.getLoginForm.get('password')
  }


  goToRegister() {
    this.route.navigate(['/authorize/register'])
  }

  onLogin(){
    console.log("user: ",this.getLoginForm.value);
  }

}
