import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Movie } from '../movie.interface';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';

const arr: Movie[] = [
  {
    id:'1',
    name:'timeng',
    type:'action',
    release:'10-25-2021',
    action: null
  },
  {
    id:'2',
    name:'hello',
    type:'camedy',
    release:'10-25-2021',
    action:null
  },
  {
    id:'3',
    name:'koko',
    type:'camedy',
    release:'10-25-2021',
    action : null
  },
  {
    id:'4',
    name:'lonely',
    type:'camedy',
    release:'10-25-2021',
    action: null
  },
  {
    id:'5',
    name:'lonely',
    type:'camedy',
    release:'10-25-2021',
    action: null
  },
  {
    id:'6',
    name:'lonely',
    type:'camedy',
    release:'10-25-2021',
    action: null
  }
]


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'type', 'release','action'];
  dataSource!: MatTableDataSource<Movie>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  constructor(
    private route: Router,
    private routes: ActivatedRoute
    ) {
    // when have arr of data, so we need to add it to MatTableDataSource first and that data can be use
    this.dataSource = new MatTableDataSource(arr);
  }

  ngOnInit(): void {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  goToUpdate(id:string) {
    this.route.navigate(['/admin/add'],{ queryParams: { movieid: id} })
  }


}
