import { AddMovieComponent } from "../add-movie/add-movie.component";
import { DashboardComponent } from "../dashboard/dashboard.component";

export const NAVBAR_ROUTES = [
    {
        path: '',
        component: DashboardComponent,
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
    },
    {
        path: 'add',
        component: AddMovieComponent,
    }
];