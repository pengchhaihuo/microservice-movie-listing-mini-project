export interface Movie {
    id: string,
    name: string,
    type: string,
    release: string,
    action:null
}
