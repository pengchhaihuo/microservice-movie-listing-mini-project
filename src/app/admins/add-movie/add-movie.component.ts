import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

  constructor(private fb: FormBuilder, private routes: ActivatedRoute) {

    this.routes.queryParams.subscribe(params => {
      this.movieId = params.movieid;
      console.log(this.movieId); // id
    })
  }

  addMovie!: FormGroup;
  movieId!: string;

  ngOnInit(): void {
    this.addMovie = this.fb.group({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      release: new FormControl('', [Validators.required]),
      type: new FormControl('', [])
    })
  }

  get getMovie() {
    return this.addMovie as FormGroup
  }

  get getTitle() {
    return this.getMovie.get('title')
  }

  get getDescription() {
    return this.getMovie.get('description')
  }

  get getRelease() {
    return this.getMovie.get('release')
  }

  get getType() {
    return this.getMovie.get('type')
  }

  onSubmit() {
    console.log("data: ", this.addMovie.value);
  }

}
