import { NavbarDashboardComponent } from "./navbar-dashboard/navbar-dashboard.component";
import { NAVBAR_ROUTES } from './navbar-dashboard/navbar.routes';


export const ADD_MOVIE_ROUTES = [
    {
        path: '',
        component: NavbarDashboardComponent,
        children: NAVBAR_ROUTES
    }
];