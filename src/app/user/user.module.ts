import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserComponent} from "./user.component";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {WatchlistComponent} from './watchlist/watchlist.component';
import {ViewdetailComponent} from './viewdetail/viewdetail.component';
import {MatButtonModule} from "@angular/material/button";
import {AllmovieComponent} from './allmovie/allmovie.component';
import {MatBadgeModule} from "@angular/material/badge";
import {HomepageComponent} from './homepage/homepage.component';
import {NgbCarouselModule} from "@ng-bootstrap/ng-bootstrap";
import {UserRouteModule} from "./user-route.module";
import {AppRoutingModule} from "../app-routing.module";


@NgModule({
  declarations: [UserComponent, WatchlistComponent, ViewdetailComponent, AllmovieComponent, HomepageComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    UserRouteModule,
    MatButtonModule,
    MatBadgeModule,
    NgbCarouselModule,
    AppRoutingModule
  ], exports: [
    UserComponent,
    MatToolbarModule,
    MatIconModule,
    AppRoutingModule
  ]
})
export class UserModule {
}
