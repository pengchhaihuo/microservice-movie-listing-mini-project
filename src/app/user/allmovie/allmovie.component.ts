import { Component, OnInit } from '@angular/core';
import {Movies} from "../../model/movies";

@Component({
  selector: 'app-allmovie',
  templateUrl: './allmovie.component.html',
  styleUrls: ['./allmovie.component.css']
})
export class AllmovieComponent implements OnInit {

  movies: Movies[]=[];
  constructor() {

  }
  ngOnInit(): void {
    this.initMovie()
  }

  //------initialized movie-------
  mv :Movies ={
    id:1,
    title:"Action Movie",
    description: "Drawn from interviews with survivors of Easy Company, as well as their journals and letters, " +
      "Band of Brothers chronicles the experiences of these men from paratrooper training in Georgia through" +
      " the end of the war. As an elite rifle company parachuting into Normandy early on D-Day morning," +
      " participants in the Battle of the Bulge, and witness to the horrors of war, the men of Easy knew" +
      " extraordinary bravery and extraordinary fear -and became the stuff of legend. Based on Stephen E." +
      " Ambrose's acclaimed book of the same name.",
    playTime:120,
    rate:9.5,
    movieUrl:"assets/demo-movie-url.jpg",
    releaseYear: "2030",
    coverImgUrl:"assets/squid-game-cover.jpg",
  }
  initMovie(){
    for(let i=0; i<40; i++){
      this.movies.push(this.mv);
    }
  }
}
