import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {HomepageComponent} from "./homepage/homepage.component";
import {AllmovieComponent} from "./allmovie/allmovie.component";
import {ViewdetailComponent} from "./viewdetail/viewdetail.component";
import {WatchlistComponent} from "./watchlist/watchlist.component";

export const userRoute: Routes = [
  {path: 'home', component: HomepageComponent},
  {path: 'movie', component: AllmovieComponent},
  {path: 'view/:id', component: ViewdetailComponent},
  {path: 'watchlist', component: WatchlistComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(userRoute)
  ], exports: [
    RouterModule
  ]
})
export class UserRouteModule {
}
