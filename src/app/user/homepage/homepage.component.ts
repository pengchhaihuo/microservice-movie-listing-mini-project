import { Component, OnInit } from '@angular/core';
import {NgbCarouselConfig} from "@ng-bootstrap/ng-bootstrap";
import {Movies} from "../../model/movies";
import {Router} from "@angular/router";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
})
export class HomepageComponent implements OnInit {

  movies: Movies[]=[];
  recommendMovie : Movies[]=[];
  constructor(private route:Router) {

  }
  ngOnInit(): void {
    this.initMovie()
  }

  //------initialized movie-------
  mv :Movies ={
    id:1,
    title:"Action Movie",
    description: "Drawn from interviews with survivors of Easy Company, as well as their journals and letters, " +
      "Band of Brothers chronicles the experiences of these men from paratrooper training in Georgia through" +
      " the end of the war. As an elite rifle company parachuting into Normandy early on D-Day morning," +
      " participants in the Battle of the Bulge, and witness to the horrors of war, the men of Easy knew" +
      " extraordinary bravery and extraordinary fear -and became the stuff of legend. Based on Stephen E." +
      " Ambrose's acclaimed book of the same name.",
    playTime:120,
    rate:9.5,
    movieUrl:"assets/demo-movie-url.jpg",
    releaseYear: "2030",
    coverImgUrl:"assets/squid-game-cover.jpg",
  }
  rcmv :Movies ={
    id:2,
    title:"Squid Game",
    description: "Drawn from interviews with survivors of Easy Company, as well as their journals and letters, " +
      "Band of Brothers chronicles the experiences of these men from paratrooper training in Georgia through" +
      " the end of the war. As an elite rifle company parachuting into Normandy early on D-Day morning," +
      " participants in the Battle of the Bulge, and witness to the horrors of war, the men of Easy knew" +
      " extraordinary bravery and extraordinary fear -and became the stuff of legend. Based on Stephen E." +
      " Ambrose's acclaimed book of the same name.",
    playTime:120,
    rate:9.5,
    movieUrl:"ddddfdd",
    releaseYear: "2030",
    coverImgUrl:"assets/squid-game-cover.jpg",
  }
  initMovie(){
    for(let i=0; i<5; i++){
      this.movies.push(this.mv);
    }
    for(let i=0; i<=5; i++){
      this.recommendMovie.push(this.rcmv);
    }
  }

  onExplore() {
    this.route.navigateByUrl("/movie");
  }
}
