import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  lableLogin!:String;
  totalNumInWatchlist :any;

  constructor(private router: Router) {
    this.lableLogin = "login"
  }

  ngOnInit(): void {
    // this.totalNumInWatchlist = null;
    this.totalNumInWatchlist = 9;
  }

  onHome() {
    this.router.navigateByUrl("/home");
  }

  onAllMovies() {
    this.router.navigateByUrl("/movie");
  }

  onWatchlist() {
    this.router.navigateByUrl("/watchlist");
  }
}
