import { Component, OnInit } from '@angular/core';
import {delay} from "rxjs/operators";
import {Movies} from "../../model/movies";
import {Router} from "@angular/router";

@Component({
  selector: 'app-viewdetail',
  templateUrl: './viewdetail.component.html',
  styleUrls: ['./viewdetail.component.css']
})
export class ViewdetailComponent implements OnInit {
  coverImg!: String;
  movie!:Movies;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.initMovie()
      this.coverImg =this.movie.coverImgUrl;
    console.log(this.movie.coverImgUrl)
  }

  //-----------init movie test---------------
  initMovie(){
    this.movie={
      id:2,
      title:"Alita Battle Angle",
      description: "Drawn from interviews with survivors of Easy Company, as well as their journals and letters, " +
        "Band of Brothers chronicles the experiences of these men from paratrooper training in Georgia through" +
        " the end of the war. As an elite rifle company parachuting into Normandy early on D-Day morning," +
        " participants in the Battle of the Bulge, and witness to the horrors of war, the men of Easy knew" +
        " extraordinary bravery and extraordinary fear -and became the stuff of legend. Based on Stephen E." +
        " the end of the war. As an elite rifle company parachuting into Normandy early on D-Day morning," +
        " participants in the Battle of the Bulge, and witness to the horrors of war, the men of Easy knew" +
        " extraordinary bravery and extraordinary fear -and became the stuff of legend. Based on Stephen E." +
        " extraordinary bravery and extraordinary fear -and became the stuff of legend. Based on Stephen E." +
        " Ambrose's acclaimed book of the same name.",
      playTime:150,
      rate:9.5,
      movieUrl:"assets/demo-movie-url.jpg",
      releaseYear: "2019",
      coverImgUrl:"assets/cover-movie2.jpg",
    }
  }

  onViewTrailer() {
    window.open("https://youtu.be/ht_XkPS__K8")
  }
  onBack(){
    history.back();
  }
}
