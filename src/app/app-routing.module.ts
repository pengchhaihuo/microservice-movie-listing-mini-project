import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AdminsComponent} from './admins/admins.component';
import {ADD_MOVIE_ROUTES} from './admins/admins.routes';
import {AuthorizationComponent} from './authorization/authorization.component';
import {AUTHORIZE_ROUTES} from './authorization/authorize.routes';
import {AuthGuard} from './authorization/guard/auth.guard';
import {UserComponent} from "./user/user.component";
import {userRoute} from "./user/user-route.module";

const routes: Routes = [
    {path: 'admin', component: AdminsComponent, children: ADD_MOVIE_ROUTES, canActivate: [AuthGuard]},
    {path: 'authorize', component: AuthorizationComponent, children: AUTHORIZE_ROUTES},
    {path:'', redirectTo: "/home",pathMatch:"full"},
    {path: '', component: UserComponent, children: userRoute},

]
;

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
